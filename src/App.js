import React from "react";
import FormInput from "./components/Form/FormInput";
import "./App.css";

function App() {
  return (
    <div>
      <FormInput />
    </div>
  );
}

export default App;