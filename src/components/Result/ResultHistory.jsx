import React from "react";

export default function ResultHistory(props) {
  return (
    <div>
      <li className="list-group-item">{props.myResultList}</li>
    </div>
  );
}
