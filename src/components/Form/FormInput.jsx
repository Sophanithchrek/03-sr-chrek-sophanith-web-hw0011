import React, { Component } from "react";
import ResultHistory from "../Result/ResultHistory";

export default class FormInput extends Component {
  // Constructor
  constructor(props) {
    super(props);
    this.state = {
      operatorSign: "+",
      txtNumber1: "",
      txtNumber2: "",
      listOfNumber: [],
    };
  }

  // Method to calculation data
  handleToCalculationData = (evt) => {
    var value1 = 0;
    var value2 = 0;
    var result = 0;

    // Condition for detect if user don't input any value
    if (this.state.txtNumber1 === "" || this.state.txtNumber2 === "") {
      alert("Please input any value!");
    } else {
      // Condition for detect if user input data as text
      if (isNaN(this.state.txtNumber1) || isNaN(this.state.txtNumber2)) {
        alert("Plese input value as number!");
      } else {
        value1 = parseInt(this.state.txtNumber1);
        value2 = parseInt(this.state.txtNumber2);

        switch (this.state.operatorSign) {
          case "+":
            result = value1 + value2;            
            break;
          case "-":
            result = value1 - value2;            
            break;
          case "*":
            result = value1 * value2;
            alert(result);
            break;
          case "/":
            result = value1 / value2;            
            break;
          case "%":
            result = value1 % value2;            
            break;
          default:            
            break;
        }
      }

      // After calculation set item into array: "listOfNumber" by value from "result"
      this.setState({
        listOfNumber: this.state.listOfNumber.concat(result),
      });
    }
  };

  handleChange = (data) => {
    this.setState({ [data.target.name]: data.target.value });
  };

  render() {
    var myList = this.state.listOfNumber.map((itm, index) => (
      // <ResultHistory myResultList={itm} key={index} />
      <ResultHistory myResultList={itm} key={index} />
    ));

    return (
      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <div className="card">
              <div className="card-body">
                <div>
                  <img
                    className="img-fluid"
                    src="images/Caculate.png"
                    alt="Image"
                  />
                </div>
                <div className="form">
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      name="txtNumber1"
                      onChange={(data) => this.handleChange(data)}
                      value={this.state.txtNumber1}
                    />
                    <input
                      type="text"
                      className="form-control my-2"
                      name="txtNumber2"
                      onChange={(data) => this.handleChange(data)}
                      value={this.state.txtNumber2}
                    />
                    <select
                      onChange={(data) => this.handleChange(data)}
                      name="operatorSign"
                      className="form-control"
                    >
                      <option value="+">+ Plus</option>
                      <option value="-">- Substract</option>
                      <option value="*">* Multiply</option>
                      <option value="/">/ Divide</option>
                      <option value="%">% Modulus</option>
                    </select>
                    <button
                      onClick={(evt) => this.handleToCalculationData(evt)}
                      className="btn btn-primary my-2"
                      type="submit"
                    >
                      Calculate
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <h1>Result history: </h1>
            <ul className="list-group">{myList}</ul>
          </div>
        </div>
      </div>
    );
  }
}
